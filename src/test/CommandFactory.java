package test;

import test.commands.Command;
import test.commands.DumpCommand;
import test.commands.GetCommand;
import test.commands.NotRecognizedCommand;
import test.commands.PutCommand;
import test.commands.QuitCommand;
import test.commands.StateCommand;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class CommandFactory{
static final String QUIT_COMMAND = "quit";
static final String DUMP_COMMAND = "dump";
static final String STATE_COMMAND = "state";

static final String PUT_COMMAND_PATTERN = "^put [1-9]\\d{0,3} [1-9]\\d{0,3}$";
static final String GET_COMMAND_PATTERN = "^get [1-9]\\d{0,5}$";

public static Command createCommand( String input ){
	Command command = new NotRecognizedCommand( input );
	switch( input ){
		case QUIT_COMMAND:
			command = new QuitCommand( );
			break;
		case DUMP_COMMAND:
			command = new DumpCommand( );
			break;
		case STATE_COMMAND:
			command = new StateCommand( );
			break;
		default:
			Pattern pattern = Pattern.compile( "[1-9]\\d{0,5}" );
			Matcher matcher = pattern.matcher( input );

			if( input.matches( PUT_COMMAND_PATTERN ) ){
				int d = 0, count = 0;
				try{
					matcher.find( );
					d = Integer.parseInt( matcher.group( ) );
					matcher.find( );
					count = Integer.parseInt( matcher.group( ) );
				}catch( NumberFormatException ignored ){ }
				if( CashMachine.DENOMINATION_TO_IDX.get( d ) != null && count <= CashMachine.MAX_BILLS_IN_CASSETTE ) command = new PutCommand( d, count );
			}

			if( input.matches( GET_COMMAND_PATTERN ) ){
				matcher.find( );
				int amount = CashMachine.MAX_NUMBER_TO_GET + 1;
				try{
					amount = Integer.parseInt( matcher.group( ) );
				}catch( NumberFormatException ignored ){ }
				if( amount <= CashMachine.MAX_NUMBER_TO_GET ) command = new GetCommand( amount );
			}
			break;
	}
	return command;
}
}