package test;

import test.commands.Command;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CashMachine{
public static final int NUMBER_OF_BILLS = 10;
public static final int MAX_NUMBER_TO_GET = 150_000;
public static final int MAX_BILLS_IN_CASSETTE = 2500;
public static final int[] DENOMINATION = new int[]{ 1, 3, 5, 10, 25, 50, 100, 500, 1000, 5000 };
public static final Map<Integer, Integer> DENOMINATION_TO_IDX = new HashMap<>( NUMBER_OF_BILLS );

private static int[] bills = new int[NUMBER_OF_BILLS];
private static int[] dp = new int[MAX_NUMBER_TO_GET + 1];
private static Long money = 0L;

public static void main( String[] args ) throws Exception{
	Command.setDp( dp );
	Command.setBills( bills );
	Command.setMoney( money );
	for( int i = 0; i < NUMBER_OF_BILLS; ++i ) DENOMINATION_TO_IDX.put( DENOMINATION[i], i );
	dp[0] = 1;

	/*
		Использовал Dynamic Programming на подобии knapsack задачи.
		dp[i] = 0 если не можем собрать данную сумму, индекс(+1) суммы с которой пришли в противном случаи.

		MAX_NUMBER_TO_GET - нагуглил, что больше 150к в день снимать нельзя

		Вкусняшки здесь:
			PutCommand#updateDp()
			GetCommand#rebuildDp()
	*/

	Scanner scanner = new Scanner( System.in );
	try{
		while( true ){
			String input = scanner.nextLine( ).trim( );
			Command command = CommandFactory.createCommand( input );
			System.out.println( ">" + input );
			command.action( );
		}
	}catch( InterruptedException exit ){ }
}
}