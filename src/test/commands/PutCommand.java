package test.commands;

import test.CashMachine;

public class PutCommand extends Command{
public static int maxIdxInDp = 1;
private int d, count;

public PutCommand( int d, int count ){
	this.d = d;
	this.count = count;
}

private static void updateDp( int bill, int count ){
	int j, i;
	for( j = 0; j < count; ++j ){
		maxIdxInDp = Math.min( maxIdxInDp + bill, CashMachine.MAX_NUMBER_TO_GET );
		for( i = maxIdxInDp; i > 0; --i )
			if( dp[i] == 0 && i - bill > -1 && dp[i - bill] != 0 ) dp[i] = i - bill + 1; // faster
//			if( i - bill > -1 && dp[i - bill] != 0 && ( dp[i - bill] > i - bill - bill || dp[i] == 0 ) ) dp[i] = i - bill + 1; // better

		/*
			Если стараться выдать наличные меньшим количеством купюр (что, зачастую, лучше) то вариант более медленный. ( > в 2 раза)
			В условии это не было оговорено, так что оставил оба
		*/

	}
}

public void action( ){
	int idx = CashMachine.DENOMINATION_TO_IDX.get( d );
	bills[idx] += count;
	money += 1L * d * count;
	updateDp( d, count );
	System.out.println( money );
}
}
