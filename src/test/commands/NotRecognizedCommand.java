package test.commands;

public class NotRecognizedCommand extends Command{
private String typed;

public NotRecognizedCommand( String typed ){ this.typed = typed; }

public void action( ){
	System.out.println( "\"" + typed + "\" is not recognized as an internal or external command :(" );
}
}
