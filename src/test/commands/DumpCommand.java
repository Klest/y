package test.commands;

import test.CashMachine;

public class DumpCommand extends Command{

public void action( ){
	for( int i = 0; i < CashMachine.NUMBER_OF_BILLS; ++i )
		System.out.println( CashMachine.DENOMINATION[i] + " " + bills[i] );
}
}
