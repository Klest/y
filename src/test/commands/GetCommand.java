package test.commands;

import test.CashMachine;

import java.util.Arrays;

public class GetCommand extends Command{
private int amount;

public GetCommand( int amount ){
	this.amount = amount;
}

private static void rebuildDp( ){
	int max = 1, i, j, k, bill;
	Arrays.fill( dp, 1, PutCommand.maxIdxInDp, 0 );
	for( k = 9; k > -1; --k ){
		bill = CashMachine.DENOMINATION[k];
		for( j = 0; j < bills[k]; ++j ){
			max = Math.min( max + bill, CashMachine.MAX_NUMBER_TO_GET - 1 );
			for( i = max; i > 0; --i )
				if( dp[i] == 0 && i - bill > -1 && dp[i - bill] != 0 ) dp[i] = i - bill + 1;
		}
	}
	PutCommand.maxIdxInDp = max;
}

public void action( ){
	int idx = amount;
	while( idx > 0 && dp[idx] == 0 ) --idx;

	int[] output = new int[CashMachine.NUMBER_OF_BILLS];
	int total = 0;
	while( dp[idx] != idx + 1 ){
		int bill = idx - dp[idx] + 1;
		idx = dp[idx] - 1;
		int idx2 = CashMachine.DENOMINATION_TO_IDX.get( bill );
		++output[idx2];
		--bills[idx2];
		total += bill;
	}
	money -= total;

	for( idx = 0; idx < CashMachine.NUMBER_OF_BILLS; ++idx )
		if( output[idx] != 0 ) System.out.print( CashMachine.DENOMINATION[idx] + "=" + output[idx] + "," );
	System.out.println( " всего " + total );
	if( total < amount ) System.out.println( "без " + ( amount - total ) );

	rebuildDp( );
}
}
