package test.commands;

public abstract class Command{
protected static int[] dp;
protected static int[] bills;
protected static Long money;

public abstract void action( ) throws InterruptedException;

public static void setBills( int[] bills ){ Command.bills = bills; }

public static void setDp( int[] dp ){ Command.dp = dp; }

public static void setMoney( Long money ){ Command.money = money; }
}
